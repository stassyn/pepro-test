CURRENT_PATH = $(shell pwd)

VENV_VERSION = 13.0.3
VENV_URL = https://pypi.python.org/packages/source/v/virtualenv/virtualenv-$(VENV_VERSION).tar.gz
VENV_PATH_PY3 = venv/py3
VENV_PATH_PY2 = venv/py2

PG_VERSION = 9.4
PG_HBA_PATH = /etc/postgresql/$(PG_VERSION)/main/pg_hba.conf
PG_CONF_PATH = /etc/postgresql/$(PG_VERSION)/main/postgresql.conf

include env_defaults
-include .env

all: \
	var/make \
	var/run \
	var/tmp \
	var/make/ \
	virtualenv-$(VENV_VERSION) \
	$(VENV_PATH_PY3) \
	$(VENV_PATH_PY2) \
	bin \
	var/make/postgres-apt-source \
	var/make/node-ppa \
	var/make/system-packages \
	var/make/ruby-gems \
	var/make/npm-packages \
	var/make/bower-packages \
	var/make/postgres-pg-hba \
	var/make/postgres-conf \
	var/make/postgres-restart \
	var/make/create-db \
	var/make/pip-packages-py2 \
	var/make/pip-packages-py3 \
	bin/redis-server \
	bin/nginx \
	var/make/crontab

var/make:
	mkdir -p var/make

var/run:
	mkdir -p var/run

var/tmp:
	mkdir -p var/tmp

var/venv:
	mkdir -p venv

var/make/postgres-apt-source:
	@if [ ! -f /etc/apt/sources.list.d/pgdg.list ]; then \
		sudo mkdir -p /etc/apt/sources.list.d/; \
		echo 'deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main' | sudo tee --append /etc/apt/sources.list.d/pgdg.list; \
		sudo apt-get update; \
	fi;
	touch $@

var/make/node-ppa:
	@if [ ! -f /etc/apt/sources.list.d/nodesource.list ]; then \
		sudo add-apt-repository -y -r ppa:chris-lea/node.js; \
		sudo rm -f /etc/apt/sources.list.d/chris-lea-node_js-*.list; \
		curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -; \
		echo 'deb https://deb.nodesource.com/node_4.x trusty main\ndeb-src https://deb.nodesource.com/node_4.x trusty main' | sudo tee /etc/apt/sources.list.d/nodesource.list; \
		sudo apt-get update; \
	fi;
	touch $@

var/make/postgres-pg-hba: $(PG_HBA_CONF)
	sudo bin/python -u helpers/replace_placeholder.py "$(PG_HBA_PATH)" "$(PG_HBA_CONF)" "PEPRO" "top" > var/tmp/pg_hba.conf
	cat var/tmp/pg_hba.conf | sudo tee $(PG_HBA_PATH) > /dev/null
	rm var/tmp/pg_hba.conf
	touch $@

var/make/postgres-conf: $(PG_CONF)
	sudo bin/python -u helpers/replace_placeholder.py "$(PG_CONF_PATH)" "$(PG_CONF)" "PEPRO" "bottom" > var/tmp/pg_conf.conf
	cat var/tmp/pg_conf.conf | sudo tee $(PG_CONF_PATH) > /dev/null
	rm var/tmp/pg_conf.conf
	touch $@


var/make/postgres-restart: $(PG_CONF) $(PG_HBA_CONF)
	sudo /etc/init.d/postgresql restart
	touch $@

var/make/crontab: .env env_defaults
	@touch var/tmp/crontab
	@crontab -l && crontab -l || echo -n "" > var/tmp/crontab
	@bin/python -u helpers/replace_placeholder.py "var/tmp/crontab" "$(CRONTAB)" "PEPRO" "bottom" | crontab
	@rm var/tmp/crontab
	@touch $@

var/make/create-db: .env env_defaults
	@if ! psql -h $(POSTGRES_HOST) -p $(POSTGRES_PORT) -U postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='$(POSTGRES_USERNAME)'" | grep -q 1; then \
        createuser -h $(POSTGRES_HOST) -p $(POSTGRES_PORT) \
            -U postgres \
            -d -e -E -l -w -r -s \
            $(POSTGRES_USERNAME) ;\
	fi ;
	@if ! psql -h $(POSTGRES_HOST) -U postgres -p $(POSTGRES_PORT) -lqt | cut -d \| -f 1 | grep -w $(POSTGRES_DB_NAME); then \
        createdb -E utf8 -h $(POSTGRES_HOST) -p $(POSTGRES_PORT) \
            --lc-collate=en_US.UTF-8 \
            --lc-ctype=en_US.UTF-8 \
            --encoding=UTF-8 \
            --template=template0 \
            -U $(POSTGRES_USERNAME) \
            $(POSTGRES_DB_NAME) ;\
	fi ;
	@touch $@

bin/nginx:
	@if [ ! -f var/tmp/nginx.tar.gz ]; then \
		wget http://nginx.org/download/nginx-1.9.2.tar.gz -O var/tmp/nginx.tar.gz; \
	fi;
	@if [ ! -d var/tmp/nginx/ ]; then \
		mkdir -p var/tmp/nginx; \
		tar xvfz var/tmp/nginx.tar.gz -C var/tmp/nginx/ --strip-components 1; \
	fi;
	cd var/tmp/nginx/ && ./configure \
		--with-http_ssl_module \
		--with-http_stub_status_module \
		--with-http_spdy_module \
		--with-http_perl_module \
		--with-http_gunzip_module \
		--with-http_gzip_static_module \
		--prefix=$(CURRENT_PATH) \
		--sbin-path=$(CURRENT_PATH)/bin \
		--conf-path=$(CURRENT_PATH)/etc/nginx/nginx.conf \
		--error-log-path=$(CURRENT_PATH)/var/log/nginx-error.log \
		--http-log-path=$(CURRENT_PATH)/var/log/nginx-access.log \
		--pid-path=$(CURRENT_PATH)/var/run/nginx.pid \
		--lock-path=$(CURRENT_PATH)/var/lock/nginx.lock
	cd var/tmp/nginx/ && sudo make install
	sudo rm -rf $(CURRENT_PATH)/html
	sudo rm -rf var/tmp/nginx*
	sudo chown -R $(SYSTEM_USER):$(SYSTEM_USER) .

bin/redis-server:
	@if [ ! -f var/tmp/redis.tar.gz ]; then \
		wget http://download.redis.io/redis-stable.tar.gz -O var/tmp/redis.tar.gz; \
	fi;
	@if [ ! -d var/tmp/redis/ ]; then \
		mkdir -p var/tmp/redis/; \
		tar xvfz var/tmp/redis.tar.gz -C var/tmp/redis/ --strip-components 1; \
	fi;
	cd $(CURRENT_PATH)/var/tmp/redis/ && make
	cp $(CURRENT_PATH)/var/tmp/redis/src/redis-server $(CURRENT_PATH)/bin/redis-server
	cp $(CURRENT_PATH)/var/tmp/redis/src/redis-cli $(CURRENT_PATH)/bin/redis-cli
	sudo rm -rf $(CURRENT_PATH)/var/tmp/redis*
	sudo chown -R $(SYSTEM_USER):$(SYSTEM_USER) .

var/make/system-packages: dependencies/apt.txt
	sudo apt-get install --force-yes -y `cat dependencies/apt.txt`
	touch $@

var/make/ruby-gems: dependencies/gems.txt
	sudo gem install `cat dependencies/gems.txt`
	touch $@

var/make/npm-packages: dependencies/npm.txt
	sudo npm install `cat dependencies/npm.txt`
	touch $@

var/make/bower-packages: dependencies/bower.txt
	./node_modules/bower/bin/bower install `cat dependencies/bower.txt`
	touch $@

virtualenv-$(VENV_VERSION):
	wget $(VENV_URL) -O- | tar -xzf- -C .

$(VENV_PATH_PY3):
	virtualenv-$(VENV_VERSION)/virtualenv.py $(VENV_PATH_PY3) -p /usr/bin/python3

$(VENV_PATH_PY2):
	virtualenv-$(VENV_VERSION)/virtualenv.py $(VENV_PATH_PY2) -p /usr/bin/python2.7

bin:
	ln -s $(VENV_PATH_PY3)/bin bin

var/make/pip-packages-py3: dependencies/pip_py3.txt setup.py
	bin/pip install -e .
	bin/pip install -r dependencies/pip_py3.txt
	touch $@

var/make/pip-packages-py2: dependencies/pip_py2.txt
	$(VENV_PATH_PY2)/bin/pip install -r dependencies/pip_py2.txt
	touch $@

clean:
	rm -rf $(VENV_PATH) $(VENV_PATH_PY2) bin virtualenv-* *.egg-info var

static:
	bin/django collectstatic --noinput --ignore=ycssmin

reload:
	touch var/run/uwsgi_reload

backup_db:
	mkdir -p var/backups/ && \
	pg_dump --clean -U $(POSTGRES_USERNAME) -Fc $(POSTGRES_DB_NAME) | gzip > var/backups/`date +%Y_%m_%d__%H_%m`.gz && \
	find var/backups/ -type f -mtime +3 -exec rm {} \;

.PHONY: clean static reload backup_db

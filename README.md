# Platform Endless PRO

Build is ready for Ubuntu 14.04 LTS and 16.04 LTS

Before building, you should configure locale for system. Add:

    LC_ALL=en_US.utf-8
    LANG=en_US.UTF-8

to `/etc/default/locale` and apply changes `source /etc/default/locale`.

To continue you will need `make` and `git` packages. To install:

    apt-get update && apt-get install git make

---

Get code:

    git clone USERNAME@bitbucket.org:USER/REPO.git
    
Go to downloaded project dir:

    cd project

Clone default variable file:

    cp env_default .env

Edit `.env` file based on you environment. Make sure to edit the user.

Build environment:

    make

Django management (instead of python manage.py):

    bin/django

Make sure to run migrations, loaddata (most recent .json from data directory).

Run celery, redis and django runserver for development:

    bin/app start

In production start uwsgi, nginx, redis and celery with supervisord:
    
    bin/_supervisord

Supervisorctl:

    bin/_supervisorctl
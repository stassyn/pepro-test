import os
import sys
import click
from invoke import run

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def set_env():
    configs = ['env_defaults', '.env']
    for config in configs:
        with open(os.path.join(BASE_DIR, config), 'r') as f:
            for line in f.readlines():
                if not line.strip():
                    continue
                var, value = line.split('=', 1)
                os.environ[var.strip()] = value.strip()


def django():
    """Django manage.py
    """
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pepro.settings")
    set_env()

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)


@click.group()
def app():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pepro.settings")
    set_env()


@app.command()
def start():
    os.chdir(BASE_DIR)
    run('bin/honcho start', warn=True, pty=True)


@click.command()
def supervisord():
    os.chdir(BASE_DIR)
    set_env()
    run('''
        . {BASE_DIR}/helpers/export_env_vars &&
        {BASE_DIR}/venv/py2/bin/supervisord -c \\
        {BASE_DIR}/{SUPERVISOR_CONF}
    '''.format(
        BASE_DIR=BASE_DIR,
        SUPERVISOR_CONF=os.environ.get('SUPERVISOR_CONF'),
    ))


@click.command(context_settings=dict(
    ignore_unknown_options=True,
))
@click.argument('supervisor_args', nargs=-1, type=click.UNPROCESSED)
def supervisorctl(supervisor_args):
    os.chdir(BASE_DIR)
    set_env()
    command = (
        '. {BASE_DIR}/helpers/export_env_vars && '
        '{BASE_DIR}/venv/py2/bin/supervisorctl '
        ' -c {BASE_DIR}/{SUPERVISOR_CONF}'
    ).format(
        BASE_DIR=BASE_DIR,
        SUPERVISOR_CONF=os.environ.get('SUPERVISOR_CONF'),
    )
    args = [
        command
    ]
    args += list(supervisor_args)
    run(' '.join(args))

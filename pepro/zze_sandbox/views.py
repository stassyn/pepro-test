from django.http import JsonResponse
from django.shortcuts import render


def index(request):
    context = {
        'foo': 'bar',
    }
    return render(request, 'sandbox/index.jinja', context)


def post_test(request):
    return JsonResponse(request.POST)

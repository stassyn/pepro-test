<sample>
    <h3>{ message }</h3>
    <ul>
        <li each={ techs }>{ name }</li>
    </ul>
    <code>
        { post_data }
    </code>

    <script>
        var self = this
        this.message = 'Hello, Riot!'
        this.techs = [
            { name: 'HTML' },
            { name: 'JavaScript' },
            { name: 'CSS' }
        ]
        data = {
            'foo': 'bar'
        }
        this.post_data = 'posting..'
        $.post('/sandbox/post-test/', data, res => {
            self.post_data = JSON.stringify(res)
            self.update()
            }, 'json')
    </script>

    <style>
        @import 'base.scss';
        $red: red;
    </style>

    <style scoped>
        h3 {
            color: $primary-color;
            @include border-radius(10px);
        }
        ul {
            color: $red;
        }
    </style>
</sample>

<top-menu>
    <nav>
      <ul class="menu">
          <li class="menu__item" each={ item in menu_items }>{ item.name }</li>
      </ul>
      <pre>{menu_items}</pre>
    </nav>
    <script>
        var self = this
        this.menu_items = [
            { name: 'HRS' }
        ]
        data =
          [
              { name: 'name', value:'HRR'},
              { name: 'Contacts' },
              { 'name': 'Notes' },
              { name: 'Projects' },
              { name: 'Leads' },
              { name: 'Posted' }
          ]

        this.post_data = 'posting..'
        $.post('/sandbox/post-test/', data, res => {
            self.menu_items = res
            self.update()
            }, 'json')
    </script>

    <style>
        @import 'base.scss';
        $red: red;
        .menu__item {
          display: inline;
        }
    </style>

    <style scoped>
        h3 {
            color: $primary-color;
            @include border-radius(10px);
        }
        ul {
            color: $red;
        }
    </style>
</top-menu>

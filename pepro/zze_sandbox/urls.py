from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^index/$', views.index, name='sandbox'),
    url(r'^post-test/$', views.post_test, name='post_test'),
]

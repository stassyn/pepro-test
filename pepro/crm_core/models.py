from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField
from django_countries.fields import CountryField
from allauth.account.signals import user_signed_up
from django.dispatch import receiver

from model_utils import Choices
from datetime import timedelta
import uuid


@receiver(user_signed_up, dispatch_uid="create_contact_on_user_signed_up")
def user_signed_up_(request, user, **kwargs):
    """
    Create new Contact for new user
    :param request:
    :param user:
    :param kwargs:
    :return:
    """
    contact = Contact(user=user)
    contact.save()


class EmailTemplate(models.Model):
    title = models.CharField(max_length=255, blank=True, default='Registration Confirmation')
    template = models.TextField()


class Address(models.Model):
    street_address_1 = models.CharField(max_length=63, blank=True,
                                        verbose_name=_("Street Address 1"))
    street_address_2 = models.CharField(max_length=63, blank=True,
                                        verbose_name=_("Street Address 2"))
    city = models.CharField(max_length=31, blank=True, verbose_name=_("City"))
    postal_code = models.CharField(max_length=11, blank=True, verbose_name=_("Postal Code"))
    country = CountryField(blank_label=_('Select Country'))
    contact = models.ForeignKey('Contact', related_name="addresses", on_delete=models.PROTECT,
                                verbose_name=_("Contact"))
    company = models.ForeignKey('Client', related_name="addresses", on_delete=models.PROTECT,
                                verbose_name=_("Client"), null=True)

    class Meta:
        verbose_name = _("Address")
        verbose_name_plural = _("Addresses")
    def __str__(self):
        return '{}\n{}\n{} {}\n{}'.format(self.street_address_1, self.street_address_2,
                                          self.postal_code, self.city, self.country.name)


class Contact(models.Model):
    TITLE_CHOICES = Choices(
        ('none', ''),
        ('Mr.', _('Mr.')),
        ('Ms.', _('Ms.')),
        ('Mrs.', _('Mrs.')),
        ('Dr.', _('Dr.')),
    )

    MATERIAL_STATUS_CHOICES = Choices(
        ('unknown', ''),
        ('Single', _('Single')),
        ('Married', _('Married')),
        ('Divorced', _('Divorced')),
        ('Widow', _('Widow')),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="contacts")
    title = models.CharField(max_length=7, blank=True, verbose_name=_("Title"),
                             choices=TITLE_CHOICES, default=TITLE_CHOICES.none)
    hide_username = models.BooleanField(default=False, blank=True)
    phone_mobile = PhoneNumberField(blank=True, verbose_name=_("Mobile Phone"))
    phone_landline = PhoneNumberField(blank=True, verbose_name=_("Landline Phone"))
    phone_fax = PhoneNumberField(blank=True, verbose_name=_("Fax"))
    #TODO: limit to self or own company for the address
    address = models.ForeignKey(Address, related_name="contacts", null=True,
                                on_delete=models.PROTECT, verbose_name=_("Address"))
    gender = models.CharField(max_length=7, blank=True, verbose_name=_("Gender"),
                              choices=(("male", _("Male")), ("female", _("Female"))))
    marital_status = models.CharField(max_length=15, blank=True, verbose_name=_("Marital Status"),
                                      choices=MATERIAL_STATUS_CHOICES,
                                      default=MATERIAL_STATUS_CHOICES.unknown)
    birthday = models.DateField(verbose_name=_("Birthday"), blank=True, null=True)
    # used for disabling user login, not for availability
    active_from = models.DateField(verbose_name=_("Active From"), blank=True, null=True,
                                   help_text=_("Date from when contact will become active"))
    # used for disabling user login, not for availability
    active_until = models.DateField(verbose_name=_("Active Until"), blank=True, null=True,
                                    help_text=_("Date from when contact will become inactive"))
    agency_notes = models.TextField(verbose_name=_("Agency Notes"), blank=True,
                                    help_text=_("Only visible to the Agency"))
    notes = models.TextField(verbose_name=_("Notes"), blank=True,
                             help_text=_("Visible to the Contact and the Agency"))
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    created_at = models.DateTimeField(verbose_name=_("Created at"), auto_now_add=True)
    available = models.BooleanField(verbose_name=_("Available"), default=True)

    class Meta:
        verbose_name = _("Contact")
        verbose_name_plural = _("Contacts")

    @property
    def display_name(self):
        return '{} {}'.format(self.user.first_name, self.user.last_name) if self.hide_username else self.user.username

    def is_available(self):
        return True #TODO: check fror ContactAvailability class

    def __str__(self):
        return '{} {} {}'.format(self.title, self.user.first_name, self.user.last_name)


class Agency(models.Model):
    agency_name = models.CharField(max_length=63, verbose_name=_("Agency Name"))
    manager = models.ForeignKey(Contact, on_delete=models.PROTECT, related_name="agencies",
                                verbose_name=_("Manager"))
    abn = models.CharField(max_length=31, verbose_name=_("ABN"), null=True,
                           help_text=_("Australian Business Number"))
    address = models.ForeignKey(Address, related_name="agencies", null=True,
                                on_delete=models.PROTECT, verbose_name=_("Address"))
    phone_office = PhoneNumberField(blank=True, verbose_name=_("Office Phone"))
    phone_fax = PhoneNumberField(blank=True, verbose_name=_("Fax"))
    website = models.URLField(verbose_name=_("Website"), blank=True)
    date_of_incorporation = models.DateField(verbose_name=_("Date of Incorporation"), null=True,
                                             blank=True)
    myob_card_number = models.CharField(max_length=31, verbose_name=_("MYOB Card Number"),
                                        blank=True)
    agency_notes = models.TextField(verbose_name=_("Agency Notes"), blank=True,
                             help_text=_("Visible to the Agency"))

    class Meta:
        verbose_name = _("Agency")
        verbose_name_plural = _("Agencies")

    def __str__(self):
        return self.agency_name


class AgencyContact(models.Model):
    contact = models.OneToOneField(Contact, on_delete=models.CASCADE,
                                   related_name="agency_contacts", verbose_name=_("Contact"))
    job_title = models.CharField(max_length=31, blank=True, verbose_name=_("Job title"))
    spouce_name = models.CharField(max_length=63, blank=True, verbose_name=_("Spouce/Partner name"))
    children = models.PositiveSmallIntegerField(verbose_name=_("Children"), default=0)
    agency = models.ForeignKey(Agency, on_delete=models.PROTECT, related_name="agency_contacts",
                               verbose_name=_("Agency"))

    class Meta:
        verbose_name = _("Agency Contact")
        verbose_name_plural = _("Agency Contacts")

    def __str__(self):
        return '{} {}'.format(self.job_title, self.contact)


class AgencySubCompany(models.Model):
    company_name = models.CharField(max_length=127, verbose_name=_("Company Name"))
    agency = models.ForeignKey(Agency, on_delete=models.PROTECT, null=True,
                               related_name="agency_sub_companies", verbose_name=_("Agency"))
    abn = models.CharField(max_length=31, verbose_name=_("ABN"), null=True,
                           help_text=_("Australian Business Number"))
    address = models.ForeignKey(Address, related_name="agency_sub_companies", null=True,
                                on_delete=models.PROTECT, verbose_name=_("Address"))
    phone_office = PhoneNumberField(blank=True, verbose_name=_("Office Phone"))
    phone_fax = PhoneNumberField(blank=True, verbose_name=_("Fax"))
    date_of_incorporation = models.DateField(verbose_name=_("Date of Incorporation"), null=True,
                                             blank=True)
    myob_card_number = models.CharField(max_length=31, verbose_name=_("MYOB Card Number"),
                                        blank=True)
    agency_notes = models.TextField(verbose_name=_("Agency Notes"), blank=True,
                             help_text=_("Visible to the Agency"))

    class Meta:
        verbose_name = _("Agency Sub-Company")
        verbose_name_plural = _("Agencies Sub-Companies")

    def __str__(self):
        return self.company_name


class Industry(models.Model):
    industry_name = models.CharField(max_length=63, verbose_name=_("Industry Name"))

    class Meta:
        verbose_name = _("Industry")
        verbose_name_plural = _("Industries")

    def __str__(self):
        return self.industry_name


class Client(models.Model):
    CREDIT_CHECK_CHOICES = Choices(
        (True, 'approved', _("Approved")),
        (False, 'not_approved', _("Not Approved")),
    )

    company_name = models.CharField(max_length=127, verbose_name=_("Company Name"))
    abn = models.CharField(max_length=31, verbose_name=_("ABN"), null=True,
                           help_text=_("Australian Business Number"))
    address = models.ForeignKey(Address, related_name="clients", null=True,
                                on_delete=models.PROTECT, verbose_name=_("Address"))
    phone_office = PhoneNumberField(blank=True, verbose_name=_("Office Phone"))
    phone_fax = PhoneNumberField(blank=True, verbose_name=_("Fax"))
    agency_contact = models.ForeignKey(AgencyContact, related_name="clients",
                              verbose_name=_("Agency Contact Responsible"),
                              on_delete=models.PROTECT, null=True)
    website = models.URLField(verbose_name=_("Website"), blank=True)
    date_of_incorporation = models.DateField(verbose_name=_("Date of Incorporation"), null=True,
                                             blank=True)
    primary_contact = models.ForeignKey(Contact, related_name="clients", on_delete=models.PROTECT,
                                        verbose_name=_("Primary Contact"), null=True)
    registered_for_gst = models.BooleanField(verbose_name=_("Registered for GST"), default=False)
    myob_card_number = models.CharField(max_length=31, verbose_name=_("MYOB Card Number"),
                                        blank=True)
    credit_check = models.BooleanField(verbose_name=_("Credit Check"), choices=CREDIT_CHECK_CHOICES,
                                       default=CREDIT_CHECK_CHOICES.not_approved)
    credit_check_date = models.DateField(verbose_name=_("Credit Check Approval Date"), null=True,
                                         blank=True)
    approved_credit_limit = models.DecimalField(max_digits=16, decimal_places=2, default=0.00,
                                                verbose_name=_("Approved Credit Limit"))
    payment_terms = models.PositiveSmallIntegerField(verbose_name=_("Payment Terms"),
                                                     help_text=_("in days"), default=0)
    description = models.TextField(verbose_name=_("Description of the Company"), blank=True)
    agency_notes = models.TextField(verbose_name=_("Agency Notes"), blank=True,
                                    help_text=_("Only visible to the Agency"))
    notes = models.TextField(verbose_name=_("Notes"), blank=True,
                             help_text=_("Visible to the Client Contacts and the Agency"))
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    created_at = models.DateTimeField(verbose_name=_("Created at"), auto_now_add=True)
    available = models.BooleanField(verbose_name=_("Available"), default=True)

    class Meta:
        verbose_name = _("Client")
        verbose_name_plural = _("Clients")

    def __str__(self):
        return self.company_name


class ClientContact(models.Model):
    contact = models.OneToOneField(Contact, on_delete=models.CASCADE,
                                   related_name="client_contacts", verbose_name=_("Contact"))
    job_title = models.CharField(max_length=31, blank=True, verbose_name=_("Job title"))
    spouce_name = models.CharField(max_length=63, blank=True, verbose_name=_("Spouce/Partner name"))
    children = models.PositiveSmallIntegerField(verbose_name=_("Children"), default=0)
    agency_contact = models.ForeignKey(AgencyContact, related_name="client_contacts",
                                       verbose_name=_("Agency Contact Responsible"),
                                       on_delete=models.PROTECT, null=True)
    client = models.ForeignKey(Client, related_name="client_contacts", on_delete=models.PROTECT,
                               verbose_name=_("Client"), null=True)

    class Meta:
        verbose_name = _("Client Contact")
        verbose_name_plural = _("Client Contacts")

    def __str__(self):
        return '{} {}'.format(self.job_title, self.contact)


class ClientState(models.Model):
    STATE_CHOICES = Choices(
        (10, 'found_lead', _("Found Lead")),
        (20, 'qualified_lead', _("Qualified Lead")),
        (30, 'needs_analysis', _("Needs Analysis")),
        (40, 'proposal_presented', _("Proposal Presented")),
        (50, 'agreement_in_progress', _("Agreement in Progress")),
        (60, 'contract_signed', _("Contract Signed")),
        (70, 'extranet_client', _("Extranet Client")),
        (80, 'extranet_revoked', _("Extranet Revoked")),
        (90, 'contract_terminated', _("Contract Terminated")),
        (0, 'sales_failed', _("Sales Failed")),
    )

    state = models.PositiveSmallIntegerField(verbose_name=_("State"), choices=STATE_CHOICES,
                                             default=STATE_CHOICES.found_lead)
    client = models.ForeignKey(Client, related_name="client_states", on_delete=models.PROTECT,
                               verbose_name=_("Client"))
    updated_by = models.ForeignKey(Contact, related_name="client_state_updates",
                                   on_delete=models.PROTECT, verbose_name=_("Updated by"))
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    notes = models.TextField(verbose_name=_("Notes"), help_text=_("State Change Description"),
                             blank=True)

    class Meta:
        verbose_name = _("Client State")
        verbose_name_plural = _("Client States")

    def __str__(self):
        return self.state


class ClientTradeReference(models.Model):
    trade_reference = models.TextField(verbose_name=_("Trade Reference"))
    client = models.ForeignKey(Client, related_name="client_trade_references",
                               on_delete=models.PROTECT, verbose_name=_("Client"))
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    referral_company_name = models.CharField(max_length=255,
                                             verbose_name=_("Company Name"))
    referral_person_name = models.CharField(max_length=255,
                                            verbose_name=_("Title, First and Last Name"))
    referral_email = models.EmailField(verbose_name=_("E-mail"))
    referral_phone = PhoneNumberField(verbose_name=_("Phone"))
    email_auth_code = models.UUIDField(default=uuid.uuid4, editable=False,
                                       verbose_name=_("E-mail authentication string"))

    class Meta:
        verbose_name = _("Client Trade Reference")
        verbose_name_plural = _("Client Trade References")

    def __str__(self):
        return _('{} from {}').format(self.client, self.referral_company_name)


class ContactAvailability(models.Model):
    STATUS_CHOICES = Choices(
        ('available', _('Available for Hire')),
        ('unused', _('Do Not use')),
    )

    contact = models.ForeignKey(Contact, related_name=("contact_availabilities"),
                                         verbose_name=_("Contact"), on_delete=models.PROTECT)
    status = models.CharField(max_length=15, verbose_name=_("Status"), choices=STATUS_CHOICES,
                              default=STATUS_CHOICES.available)
    status_from = models.DateField(verbose_name=_("From"), null=True, blank=True)
    status_until = models.DateField(verbose_name=_("Until"), null=True, blank=True)
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    updated_by = models.ForeignKey(Contact, related_name="contact_availability_updates",
                                   verbose_name=_("Updated by"), on_delete=models.PROTECT)
    notes = models.TextField(verbose_name=_("Notes"), help_text=_("Status Change Description"),
                             blank=True)

    class Meta:
        verbose_name = _("Contact Availability")
        verbose_name_plural = _("Contact Availabilities")

    def __str__(self):
        return self.status


class Activity(models.Model):
    subject = models.CharField(max_length=255, blank=True, verbose_name=_("Subject"))
    text = models.TextField(blank=True, verbose_name=_("Note"))
    starts_at = models.DateTimeField(verbose_name=_("Activity Starts at"))
    ends_at = models.DateTimeField(verbose_name=_("Activity Ends at"))
    agency_contact = models.ForeignKey(AgencyContact, related_name="activities",
                                       verbose_name=_("Agency Contact"),
                                       on_delete=models.PROTECT, null=True)
    client_contact = models.ForeignKey(ClientContact, related_name="activities",
                                       verbose_name=_("Agency Contact"),
                                       on_delete=models.PROTECT, null=True)
    #TODO: Extend for EmployeeContact in crm_hr
    client = models.ForeignKey(Client, related_name="activities", verbose_name=_("Client"),
                               on_delete=models.PROTECT, null=True)

    class Meta:
        verbose_name = _("Contact Activity")
        verbose_name_plural = _("Contact Activities")

    def duration(self):
        return timedelta(self.ends_at - self.starts_at)

    def __str__(self):
        return '{} - {}: {}'.format(self.starts_at, self.ends_at, self.subject)


class Reminder(models.Model):
    time_before_activity = models.TimeField(default=timedelta(minutes=10))
    activity = models.ForeignKey(Activity, on_delete=models.PROTECT, related_name="reminders",
                                 verbose_name="Activity")
    alarm = models.BooleanField(verbose_name=_("Alarm"), default=True)
    open_related_entity = models.BooleanField(verbose_name=_("Open Related Entity"), default=True)
    send_email_to_contacts = models.BooleanField(verbose_name=_("Send e-mail to Contacts"),
                                                 default=True)
    send_sms_to_contacts = models.BooleanField(verbose_name=_("Send SMS to Contacts"),
                                               default=False)
    send_email_to_agent = models.BooleanField(verbose_name=_("Send e-mail to Agentcy Contact"),
                                              default=True)
    send_sms_to_agent = models.BooleanField(verbose_name=_("Send SMS to Agency Contact"),
                                            default=False)

    class Meta:
        verbose_name = _("Contact Activity")
        verbose_name_plural = _("Contact Activities")

    def __str__(self):
        return self.time_before_activity


class BlogEntry(models.Model):
    title = models.CharField(max_length=255, verbose_name=_("Title"))
    contents = models.TextField(verbose_name=_("Contents"), blank=True)
    author = models.ForeignKey(Contact, on_delete=models.CASCADE, related_name='blog_entries',
                               verbose_name=_("Author"))
    created_at = models.DateTimeField(verbose_name=_("Created At"), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_("Updated At"), auto_now=True)
    slug = models.SlugField(max_length=63, verbose_name=_("Slug"))

    class Meta:
        verbose_name = _("Blog Entry")
        verbose_name_plural = _("Blog Entries")

    def __str__(self):
        return '{} - &copy; {} {}'.format(self.title, self.updated_at, self.author)

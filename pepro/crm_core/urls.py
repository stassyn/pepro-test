from django.conf.urls import url
from . import views
from allauth.account import views as allauth_views



urlpatterns = [
    # Authentication and registration
    url(r'^user/logout/$', allauth_views.LogoutView.as_view(), name='account_logout'),
    url(r'^user/login/$', allauth_views.LoginView.as_view(), name='account_login'),
    url(r'^user/register/$', allauth_views.SignupView.as_view(), name='account_signup'),
    url(r'^user/email/$', allauth_views.EmailView.as_view(), name='account_email'),
    url(r'^user/confirm-email/$', allauth_views.EmailVerificationSentView.as_view(),
        name='account_email_verification_sent'),
    url(r'^user/confirm-email/(?P<key>[-:\w]+)/$', allauth_views.ConfirmEmailView.as_view(),
        name='account_confirm_email'),

    # User data API
    url(r'^data/user/$', views.get_user_data, name='user_data'),
    url(r'^data/user/login/$', allauth_views.LoginView.as_view(), name='ajax_account_login'),
    url(r'^data/user/register/$', allauth_views.SignupView.as_view(), name='ajax_account_signup'),
]
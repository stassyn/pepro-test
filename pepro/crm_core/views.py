import json
from . import models
from django.core import serializers
from django.shortcuts import render, HttpResponse


def get_user_data(request):
    response = None
    serializable_fields = (
        'pk',
        'username',
        'first_name',
        'last_name',
        'email'
    )

    if request.user.is_authenticated():
        response = request.user

    data = serializers.serialize('json', [response], fields=serializable_fields) if response else '[]'
    return HttpResponse(
        content=data,
        content_type='application/json'
    )

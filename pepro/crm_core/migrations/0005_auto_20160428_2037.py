# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-28 10:37
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crm_core', '0004_auto_20160428_1921'),
    ]

    operations = [
        migrations.RenameField(
            model_name='client',
            old_name='agent',
            new_name='agency_contact',
        ),
        migrations.RenameField(
            model_name='clientcontact',
            old_name='agent',
            new_name='agency_contact',
        ),
    ]

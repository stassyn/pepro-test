# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-02 14:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm_core', '0011_auto_20160503_0022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clientstate',
            name='state',
            field=models.PositiveSmallIntegerField(choices=[(10, 'Found Lead'), (20, 'Qualified Lead'), (30, 'Needs Analysis'), (40, 'Proposal Presented'), (50, 'Agreement in Progress'), (60, 'Contract Signed'), (70, 'Extranet Client'), (80, 'Extranet Revoked'), (90, 'Contract Terminated'), (0, 'Sales Failed')], default=10, verbose_name='State'),
        ),
    ]

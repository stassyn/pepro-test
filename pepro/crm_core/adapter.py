#!/usr/bin/env python
# -*- coding: utf-8 -*-
from allauth.account.adapter import DefaultAccountAdapter
from allauth.utils import get_current_site
from django.core.mail import send_mail
from jinja2 import Template
from pepro.crm_core.models import EmailTemplate


class CustomAccountAdapter(DefaultAccountAdapter):
    def send_confirmation_mail(self, request, emailconfirmation, signup):
        message_template = EmailTemplate.objects.order_by('-id').first()
        message_html = Template(message_template.template)
        email_context = {
            'user': emailconfirmation.email_address.user,
            "activate_url": self.get_email_confirmation_url(request, emailconfirmation),
            "current_site": get_current_site(request),
            "key": emailconfirmation.key,
        }

        send_mail(
            message='',
            subject=message_template.title,
            from_email='account@pepro.com',
            html_message=message_html.render(**email_context),
            recipient_list=[email_context['user'].email],
            fail_silently=False
        )

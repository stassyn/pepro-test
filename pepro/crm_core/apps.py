from __future__ import unicode_literals

from django.apps import AppConfig


class CrmCoreConfig(AppConfig):
    name = 'crm_core'

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pepro.settings')

from celery import Celery
from django.conf import settings  # noqa


app = Celery('pepro')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

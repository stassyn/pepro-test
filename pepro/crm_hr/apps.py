from django.apps import AppConfig


class CrmHrConfig(AppConfig):
    name = 'crm_hr'

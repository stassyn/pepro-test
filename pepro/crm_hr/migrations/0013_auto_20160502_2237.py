# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-02 12:37
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('crm_hr', '0012_auto_20160502_2135'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmployeeTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'verbose_name': 'Employee Tag',
                'verbose_name_plural': 'Employee Tags',
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=63, verbose_name='Tag Name')),
            ],
            options={
                'verbose_name': 'Tag',
                'verbose_name_plural': 'Tags',
            },
        ),
        migrations.AddField(
            model_name='skill',
            name='hourly_rate',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=8, verbose_name='Hourly Rate'),
        ),
        migrations.AlterField(
            model_name='employeecontact',
            name='contact',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='employee_contacts', to='crm_core.Contact', verbose_name='Contact'),
        ),
        migrations.AddField(
            model_name='employeetag',
            name='employee_contact',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='employee_tags', to='crm_hr.EmployeeContact'),
        ),
        migrations.AddField(
            model_name='employeetag',
            name='tag',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='employee_tags', to='crm_hr.Skill', verbose_name='Tag'),
        ),
        migrations.AlterUniqueTogether(
            name='employeetag',
            unique_together=set([('tag', 'employee_contact')]),
        ),
    ]

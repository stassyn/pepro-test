# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-28 10:37
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crm_hr', '0003_auto_20160428_2029'),
    ]

    operations = [
        migrations.RenameField(
            model_name='employeecontact',
            old_name='agent',
            new_name='agency_contact',
        ),
        migrations.RenameField(
            model_name='employeestatus',
            old_name='employee',
            new_name='employee_contact',
        ),
    ]

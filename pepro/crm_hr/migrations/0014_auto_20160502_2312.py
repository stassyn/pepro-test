# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-02 13:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('crm_hr', '0013_auto_20160502_2237'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmployeeSkillRate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hourly_rate', models.DecimalField(decimal_places=2, default=0.0, max_digits=8, verbose_name='Hourly Rate')),
                ('valid_from', models.DateField(verbose_name='Valid From')),
                ('valid_until', models.DateField(verbose_name='Valid Until')),
            ],
            options={
                'verbose_name': 'Employee Skill Rate',
                'verbose_name_plural': 'Employee Skill Rates',
            },
        ),
        migrations.RemoveField(
            model_name='employeeskill',
            name='hourly_rate',
        ),
        migrations.AddField(
            model_name='employeeskillrate',
            name='employee_skill',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='employee_skill_rates', to='crm_hr.EmployeeSkill', verbose_name='Employee Skill'),
        ),
    ]

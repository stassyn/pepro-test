from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils import Choices
from pepro.crm_core.models import Contact, AgencyContact, Address, Client, ClientContact
from phonenumber_field.modelfields import PhoneNumberField
from datetime import date, timedelta, time


# Create your models here.
class VisaType(models.Model):
    visa_subclass = models.CharField(max_length=3, verbose_name=_("Subclass Number"))
    visa_name = models.CharField(max_length=127, verbose_name=_("Visa Type Name"))
    visa_type = models.CharField(max_length=63, verbose_name=_("General Visa Type"))

    class Meta:
        verbose_name = _("Employee Visa Type")
        verbose_name_plural = _("Employee Visa Types")

    def __str__(self):
        return '{}: {} ({})'.format(self.visa_subclasss, self.visa_name, self.visa_type)


class PrivateBankAccount(models.Model):
    bank_name = models.CharField(max_length=63, verbose_name=_("Bank Name"))
    bank_account_name = models.CharField(max_length=63, verbose_name=_("Bank Account Name"))
    bsb = models.CharField(max_length=6, verbose_name=_("BSB"))
    account_number = models.CharField(max_length=10, verbose_name=_("Account Number"))
    owner = models.ForeignKey(Contact, related_name="private_bank_accounts",
                              verbose_name=_("Owner"), on_delete=models.PROTECT)
    primary = models.BooleanField(default=False, verbose_name=_("Primary"))

    class Meta:
        verbose_name = _("Private Bank Account")
        verbose_name_plural = _("Private Bank Accounts")
        unique_together = ("bank_name", "bank_account_name", "owner")

    def __str__(self):
        return '{}: {}'.format(self.bank_name, self.bank_account_name)


class EmployeeSubContractor(models.Model):
    SUBCONTRACTOR_TYPE_CHOICES = Choices(
        (10, 'sole_trader', _("sole_trader")),
        (20, 'company', _("company")),
    )

    company_name = models.CharField(max_length=127, verbose_name=_("Company Name"))
    subcontractor_type = models.PositiveSmallIntegerField(
        verbose_name=_("Subcontractor Type"),
        choices=SUBCONTRACTOR_TYPE_CHOICES,
        default=SUBCONTRACTOR_TYPE_CHOICES.sole_trader
    )
    abn = models.CharField(max_length=31, verbose_name=_("ABN"), null=True,
                           help_text=_("Australian Business Number"))
    address = models.ForeignKey(Address, related_name="employee_subcontractors", null=True,
                                on_delete=models.PROTECT, verbose_name=_("Address"))
    phone_office = PhoneNumberField(blank=True, verbose_name=_("Office Phone"))
    phone_fax = PhoneNumberField(blank=True, verbose_name=_("Fax"))
    agency_contact = models.ForeignKey(AgencyContact, related_name="employee_subcontractors",
                              verbose_name=_("Agency Contact Responsible"),
                              on_delete=models.PROTECT, null=True)
    website = models.URLField(verbose_name=_("Website"), blank=True)
    date_of_incorporation = models.DateField(verbose_name=_("Date of Incorporation"), null=True,
                                             blank=True)
    primary_contact = models.ForeignKey(Contact, related_name="employee_subcontractors",
                                        on_delete=models.PROTECT, verbose_name=_("Primary Contact"),
                                        null=True)
    registered_for_gst = models.BooleanField(verbose_name=_("Registered for GST"), default=False)
    myob_card_number = models.CharField(max_length=31, verbose_name=_("MYOB Card Number"),
                                        blank=True)
    payment_terms = models.PositiveSmallIntegerField(verbose_name=_("Payment Terms"),
                                                     help_text=_("in days"), default=0)
    description = models.TextField(verbose_name=_("Description of the Company"), blank=True)
    agency_notes = models.TextField(verbose_name=_("Agency Notes"), blank=True,
                                    help_text=_("Only visible to the Agency"))
    notes = models.TextField(verbose_name=_("Notes"), blank=True,
                             help_text=_("Visible to the Client Contacts and the Agency"))
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    created_at = models.DateTimeField(verbose_name=_("Created at"), auto_now_add=True)
    available = models.BooleanField(verbose_name=_("Available"), default=True)

    class Meta:
        verbose_name = _("Employee Subcontractor")
        verbose_name_plural = _("Employee Subcontractors")

    def __str__(self):
        return self.company_name


class EmployeeContact(models.Model):
    RESIDENCY_STATUS_CHOICES = Choices(
        (0, 'unknown', _('Unknown')),
        (1, 'citizen', _('Citizen')),
        (2, 'permanent', _('Permanent Resident')),
        (3, 'temporary', _('Temporary Resident')),
    )
    REFERRAL_CHOICES = Choices(
        (0, 'other', _("Other / Unspecified")),
        (1, 'direct', _("Direct Contact")),
        (2, 'friend', _("Friend")),
        (3, 'internet', _("Internet Search")),
        (4, 'RTO', _("RTO")),
        (5, 'job_agent', _("Job Agency")),
        (6, 'advertisement', _("Advertisement")),
    )
    TRANSPORTATION_CHOICES = Choices(
        (1, 'own', _("Own Car")),
        (2, 'public', _("Public Transportation")),

    )

    contact = models.OneToOneField(Contact, on_delete=models.CASCADE,
                                   related_name="employee_contacts", verbose_name=_("Contact"))
    agency_contact = models.ForeignKey(AgencyContact, related_name="employee_contacts",
                                       verbose_name=_("Agency Contact Responsible"),
                                       on_delete=models.PROTECT, null=True)
    residency = models.PositiveSmallIntegerField(verbose_name=_("Residency Status"),
                                 choices=RESIDENCY_STATUS_CHOICES,
                                 default=RESIDENCY_STATUS_CHOICES.unknown)
    visa_type = models.ForeignKey(VisaType, related_name="employee_contacts",
                                  verbose_name=_("Visa Type"), blank=True, on_delete=models.PROTECT)
    visa_expiry_date = models.DateField(verbose_name=_("Visa Expiry Date"), blank=True, null=True)
    vevo_checked_at = models.BooleanField(verbose_name=_("VEVO checked"), default=False)
    referral = models.PositiveSmallIntegerField(verbose_name=_("Referral Source"),
                                choices=REFERRAL_CHOICES, default=REFERRAL_CHOICES.direct)
    tax_file_number = models.CharField(max_length=9, verbose_name=_("Tax File Number"), blank=True)
    super_annual_fund_name = models.CharField(max_length=63, blank=True,
                                              verbose_name=_("Super annual Fund Name"))
    super_member_number = models.CharField(max_length=63, verbose_name=_("Super Member Number"),
                                           blank=True)
    weight = models.DecimalField(max_digits=8, decimal_places=2, verbose_name=_("Weight"),
                                 default=75.00)
    height = models.DecimalField(max_digits=8, decimal_places=2, verbose_name=_("Height"),
                                 default=1.75)
    transportation_to_work = models.PositiveSmallIntegerField(
        choices=TRANSPORTATION_CHOICES,
        verbose_name=_("Transportation to Work"),
        default=TRANSPORTATION_CHOICES.own
    )
    emergency_contact_name = models.CharField(max_length=63,
                                              verbose_name=_("Emergency Contact Name"), blank=True)
    emergency_contact_phone = PhoneNumberField(verbose_name=_("Emergency Contact Phone Number"),
                                               blank=True)
    reliability_score = models.PositiveSmallIntegerField(verbose_name=_("Reliability Score"),
                                                         default=0)
    loyalty_score = models.PositiveSmallIntegerField(verbose_name=_("Loyalty Score"), default=0)
    total_score = models.PositiveSmallIntegerField(verbose_name=_("Total Score"), default=0)
    autoreceives_sms = models.BooleanField(verbose_name=_("Autoreceives SMS"), default=True)

    class Meta:
        verbose_name = _("Employee")
        verbose_name_plural = _("Employees")

    def recruited(self):
        last_status = self.recruitment_statuses.latest('created_at')
        if last_status:
            return last_status.is_recruited()
        else:
            return False

    def __str__(self):
        return self.contact


class EmployeeContactRecruitingStatus(models.Model):
    STATUS_CHOICES = Choices(
        (10, 'new', _('New Candidate Registered')),
        (20, 'phone_interview_passed', _('Phone Interview Passed')),
        (30, 'live_interview_passed', _('Live Interview Passed')),
        (40, 'tax_information_form_filled', _('Tax Information Form Filled')),
        (50, 'immigration_information_filled', _('Immigration Information Filled')),
        (70, 'recruited', _('Recruited - Available for Hire')),
        (90, 'retired', _('Retired')),
        (0, 'failed', _('Failed to recruit')),
    )

    employee_contact = models.ForeignKey(EmployeeContact, on_delete=models.PROTECT,
                                         related_name="recruitment_statuses",
                                         verbose_name=_("Employee Contact"))
    status = models.PositiveSmallIntegerField(verbose_name=_("Status"), choices=STATUS_CHOICES,
                              default=STATUS_CHOICES.new)
    created_at = models.DateTimeField(verbose_name=_("Status Changed"), auto_now=True)
    created_by = models.ForeignKey(Contact, related_name="employee_contact_recruitment_statuses",
                                   verbose_name=_("Changed by"), on_delete=models.PROTECT)
    notes = models.TextField(verbose_name=_("Notes"), help_text=_("Status Change Description"),
                             blank=True)

    class Meta:
        verbose_name = _("Employee Status")
        verbose_name_plural = _("Employee Statuses")

    def is_recruited(self):
        return self.status == self.STATUS_CHOICES.recruited

    def __str__(self):
        return self.status


class Skill(models.Model):
    name = models.CharField(max_length=63, verbose_name=_("Skill Name"))
    hourly_rate = models.DecimalField(decimal_places=2, max_digits=8,
                                      verbose_name=_("Hourly Rate"), default=0.00)

    class Meta:
        verbose_name = _("Skill")
        verbose_name_plural = _("Skills")

    def __str__(self):
        return self.name


class EmployeeSkill(models.Model):
    skill = models.ForeignKey(Skill, related_name="employee_skills", on_delete=models.PROTECT,
                              verbose_name=_("Skill"))
    score = models.PositiveSmallIntegerField(verbose_name=_("Score"), default=0)
    employee_contact = models.ForeignKey(EmployeeContact, on_delete=models.PROTECT,
                                         related_name="employee_skills")

    class Meta:
        verbose_name = _("Employee Skill")
        verbose_name_plural = _("Employee Skills")
        unique_together = ("skill", "employee_contact")

    def get_valid_hourly_rate(self):
        today = date.today()
        return self.employee_skill_rates.objects.filter(valid_from__gt=today,
                                                        valid_until__lt=today).last()

    def __str__(self):
        return '{} ({}*)'.format(self.skill, self.score)


class EmployeeSkillRate(models.Model):
    employee_skill = models.ForeignKey(EmployeeSkill, related_name="employee_skill_rates",
                                       on_delete=models.PROTECT, verbose_name=_("Employee Skill"))
    hourly_rate = models.DecimalField(decimal_places=2, max_digits=8, verbose_name=_("Hourly Rate"),
                                      default=0.00)
    valid_from = models.DateField(verbose_name=_("Valid From"))
    valid_until = models.DateField(verbose_name=_("Valid Until"))

    class Meta:
        verbose_name = _("Employee Skill Rate")
        verbose_name_plural = _("Employee Skill Rates")

    def __str__(self):
        return '${}/h {}'.format(self.hourly_rate, self.valid_until)


class Tag(models.Model):
    name = models.CharField(max_length=63, verbose_name=_("Tag Name"))

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")

    def __str__(self):
        return self.name


class EmployeeTag(models.Model):
    tag = models.ForeignKey(Skill, related_name="employee_tags", on_delete=models.PROTECT,
                            verbose_name=_("Tag"))
    employee_contact = models.ForeignKey(EmployeeContact, on_delete=models.PROTECT,
                                         related_name="employee_tags",
                                         verbose_name=_("Employee Contact"))

    class Meta:
        verbose_name = _("Employee Tag")
        verbose_name_plural = _("Employee Tags")
        unique_together = ("tag", "employee_contact")

    def __str__(self):
        return self.tag


class JobsiteType(models.Model):
    type = models.CharField(max_length=63, verbose_name=_("Type"))

    class Meta:
        verbose_name = _("Jobsite Type")
        verbose_name_plural = _("Jobsite Types")

    def __str__(self):
        return self.type


class Jobsite(models.Model):
    site_name = models.CharField(max_length=63, verbose_name=_("Site Name"))
    address = models.ForeignKey(Address, related_name="jobsites",
                                on_delete=models.PROTECT, verbose_name=_("Address"))
    agency_contact = models.ForeignKey(AgencyContact, related_name="jobsites",
                                       verbose_name=_("Agency Contact Responsible"),
                                       on_delete=models.PROTECT, null=True)
    client = models.ForeignKey(Client, related_name="jobsites", on_delete=models.PROTECT,
                               verbose_name=_("Client"))
    client_contact = models.ForeignKey(ClientContact, related_name="jobsites",
                                       verbose_name=_("Client Contact Responsible"),
                                       on_delete=models.PROTECT, null=True)
    available = models.BooleanField(verbose_name=_("Available"), default=True)
    agency_notes = models.TextField(verbose_name=_("Agency Notes"), blank=True,
                                    help_text=_("Only visible to the Agency"))
    notes = models.TextField(verbose_name=_("Notes"), blank=True,
                             help_text=_("Visible to the Client Contacts and the Agency"))
    start_date = models.DateField(verbose_name=_("Start Date"), null=True, blank=True)
    end_date = models.DateField(verbose_name=_("End Date"), null=True, blank=True)
    estimated_duration = models.DurationField(verbose_name=_("Estimated Duration"), null=True)
    type = models.ForeignKey(JobsiteType, on_delete=models.PROTECT, related_name="jobsites",
                             verbose_name=_("Type"))

    class Meta:
        verbose_name = _("Jobsite")
        verbose_name_plural = _("Jobsites")
        unique_together = ("address", "client")

    def __str__(self):
        return '{} / {}'.format(self.site_name, self.client)


class JobsiteAvailability(models.Model):
    STATUS_CHOICES = Choices(
        ('available', _('Available')),
        ('unused', _('Do Not use')),
    )

    jobsite = models.ForeignKey(Jobsite, related_name=("jobsite_availabilities"),
                                verbose_name=_("Jobsite"), on_delete=models.PROTECT)
    status = models.CharField(max_length=15, verbose_name=_("Status"), choices=STATUS_CHOICES,
                              default=STATUS_CHOICES.available)
    status_from = models.DateField(verbose_name=_("From"), null=True, blank=True)
    status_until = models.DateField(verbose_name=_("Until"), null=True, blank=True)
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    updated_by = models.ForeignKey(Contact, related_name="jobsite_availability_updates",
                                   verbose_name=_("Updated by"), on_delete=models.PROTECT)
    notes = models.TextField(verbose_name=_("Notes"), help_text=_("Status Change Description"),
                             blank=True)

    class Meta:
        verbose_name = _("Jobsite Availability")
        verbose_name_plural = _("Jobsite Availabilities")

    def __str__(self):
        return self.status


class JobsiteState(models.Model):
    STATE_CHOICES = Choices(
        (10, 'announced_new', _("Announced/New")),
        (30, 'preparation', _("Preparation")),
        (50, 'open', _("Open")),
        (80, 'suspended', _("Suspended")),
        (90, 'closed', _("Closed")),
        (0, 'aborted', _("Aborted")),
    )

    state = models.PositiveSmallIntegerField(verbose_name=_("State"), choices=STATE_CHOICES,
                                             default=STATE_CHOICES.announced_new)
    jobsite = models.ForeignKey(Jobsite, related_name="jobsite_states", on_delete=models.PROTECT,
                               verbose_name=_("Jobsite"))
    updated_by = models.ForeignKey(Contact, related_name="jobsite_state_updates",
                                   on_delete=models.PROTECT, verbose_name=_("Updated by"))
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    notes = models.TextField(verbose_name=_("Notes"), help_text=_("State Change Description"),
                             blank=True)

    class Meta:
        verbose_name = _("Jobsite State")
        verbose_name_plural = _("Jobsite States")

    def __str__(self):
        return self.state


class Vacancy(models.Model):
    WORK_TYPE_CHOICES = Choices(
        (10, 'temp_or_contract', _("Temporary or Contract")),
        (20, 'part_time', _("Part Time")),
        (30, 'casual', _("Casual")),
    )

    title = models.CharField(max_length=255, verbose_name=_("Title"))
    description = models.TextField(verbose_name=_("Description"), blank=True)
    jobsite = models.ForeignKey(Jobsite, related_name="vacancies", on_delete=models.CASCADE,
                                verbose_name=_("Jobsite"))
    category = models.ForeignKey(EmployeeSkill, related_name="vacancies", on_delete=models.PROTECT,
                                  verbose_name=_("Category"))
    work_type = models.PositiveSmallIntegerField(verbose_name=_("Work Type"),
                                                 choices=WORK_TYPE_CHOICES,
                                                 default=WORK_TYPE_CHOICES.temp_or_contract)
    updated_by = models.ForeignKey(Contact, related_name="vacancy_updates",
                                   on_delete=models.PROTECT,
                                   verbose_name=_("Updated by"))
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    published = models.BooleanField(default=False, verbose_name=_("Published"))
    publish_on = models.DateField(verbose_name=_("To be published on"), null=True, blank=True)
    expires_on = models.DateField(verbose_name=_("Expires on"), null=True, blank=True)

    class Meta:
        verbose_name = _("Vacancy")
        verbose_name_plural = _("Vacancies")

    def __str__(self):
        return self.title


class ClientSkillRate(models.Model):
    skill = models.ForeignKey(Skill, related_name="client_skill_rates",
                              on_delete=models.PROTECT, verbose_name=_("Skill"))
    hourly_rate = models.DecimalField(decimal_places=2, max_digits=8, verbose_name=_("Hourly Rate"),
                                      default=0.00)
    valid_from = models.DateField(verbose_name=_("Valid From"), default=date.today)
    valid_until = models.DateField(verbose_name=_("Valid Until"), null=True, blank=True)
    client = models.ForeignKey(Client, related_name="client_skill_rates", on_delete=models.PROTECT,
                               verbose_name=_("Client"))
    approved_by_client = models.BooleanField(verbose_name=_("Approved by Client"))
    approved_by_client_contact = models.ForeignKey(ClientContact, on_delete=models.PROTECT,
                                       related_name="client_skill_rates",
                                       verbose_name=_("Approved by Client Contact"))
    approved_by_client_at = models.DateTimeField(null=True, blank=True,
                                                 verbose_name=_("Approved by Client on"))
    approved_by_agency = models.BooleanField(verbose_name=_("Approved by the Agency"))
    approved_by_agency_contact = models.ForeignKey(AgencyContact, on_delete=models.PROTECT,
                                                   related_name="client_skill_rates",
                                                   verbose_name=_("Approved by Agency Contact"))
    approved_by_agency_at = models.DateTimeField(null=True, blank=True,
                                                 verbose_name=_("Approved by Agency on"))

    class Meta:
        verbose_name = _("Client Skill Rate")
        verbose_name_plural = _("Client Skill Rates")

    def __str__(self):
        return '{}: ${}/h {}'.format(self.skill, self.hourly_rate, self.valid_until)


class RateCoefficients(models.Model):
    WEEKDAY_CHOICES = Choices(
        (1, 'monday', _("Monday")),
        (2, 'tuesday', _("Tuesday")),
        (3, 'wednesday', _("Wednesday")),
        (4, 'thursday', _("Thursday")),
        (5, 'friday', _("Friday")),
        (6, 'saturday', _("Saturday")),
        (7, 'sunday', _("Sunday")),
        (0, 'bank_holiday', _("Bank Holiday")),
    )

    name = models.CharField(max_length=31, verbose_name=_("Name"))
    rate_multiplier = models.DecimalField(decimal_places=2, max_digits=4, default=1.00,
                                          verbose_name=_("Multiplier"), help_text=_("1.00 = none"))
    used_for_overtime = models.BooleanField(default=False, verbose_name=_("Used for Overtime"))
    overtime_hours_threshold = models.DurationField(verbose_name=_("Overtime Hours Threshold"),
                                                    default=0)
    used_for_weekday = models.BooleanField(default=False, verbose_name=_("Used for Weekdays"))
    weekday = models.PositiveSmallIntegerField(verbose_name=_("Weekday"), choices=WEEKDAY_CHOICES,
                                               default=WEEKDAY_CHOICES.bank_holiday)
    used_for_time_of_day = models.BooleanField(default=False,
                                               verbose_name=_("Used for Time of the Day"))
    time_start = models.TimeField(verbose_name=_("Time From"), default=time(hour=18))
    time_end = models.TimeField(verbose_name=_("Time To"), default=time(hour=6))
    valid_from = models.DateField(verbose_name=_("Valid From"), default=date.today)
    valid_until = models.DateField(verbose_name=_("Valid Until"), null=True, blank=True)
    updated_by = models.ForeignKey(Contact, related_name="rate_coefficient_updates",
                                   on_delete=models.PROTECT,
                                   verbose_name=_("Updated by"))
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    effective = models.BooleanField(verbose_name=_("Effective"), default=False)

    class Meta:
        verbose_name = _("Rate Coefficient")
        verbose_name_plural = _("Rate Coefficients")

    def __str__(self):
        return '{}: {}'.format(self.name, self.rate_multiplier)


class ClientRateCoefficient(models.Model):
    client = models.ForeignKey(Client, related_name="client_rate_coefficients",
                               on_delete=models.PROTECT, verbose_name=_("Client"))
    rate_coefficient = models.ForeignKey(RateCoefficients, related_name="client_rate_coefficients",
                                         on_delete=models.PROTECT,
                                         verbose_name=_("Rate Coefficient"))

    class Meta:
        verbose_name = _("Client Rate Coefficient")
        verbose_name_plural = _("Client Rate Coefficients")
        unique_together = ("client", "rate_coefficient")

    def __str__(self):
        return '{}: {}'.format(self.client, self.rate_coefficient)


class BookingsInvoice(models.Model):
    client = models.ForeignKey(Client, related_name="booking_invoices", on_delete=models.PROTECT,
                               verbose_name=_("Client"))
    client_representative = models.ForeignKey(ClientContact, related_name="bookings_invoices",
                                              on_delete=models.PROTECT,
                                              verbose_name=_("Client Representative"))
    client_representative_signed_at = models.DateTimeField(null=True, blank=True,
                                                           verbose_name=_("Client Signed at"))
    agency_representative = models.ForeignKey(AgencyContact, related_name="bookings_invoices",
                                              on_delete=models.PROTECT,
                                              verbose_name=_("Agency Representative"))
    agency_representative_signed_at = models.DateTimeField(null=True, blank=True,
                                                       verbose_name=_("Agency Signed at"))

    class Meta:
        verbose_name = _("Booking")
        verbose_name_plural = _("Bookings")

    def __str__(self):
        return '{}: {} {}, {} {}'.format(self.client, self.client_representative,
                                         self.client_representative_signed_at,
                                         self.agency_representative,
                                         self.agency_representative_signed_at)


class Booking(models.Model):
    client = models.ForeignKey(Client, related_name="bookings", on_delete=models.PROTECT,
                               verbose_name=_("Client"))
    employee_contact = models.ForeignKey(EmployeeContact, on_delete=models.PROTECT,
                                         related_name="bookings",
                                         verbose_name=_("Employee Contact"))
    jobsite = models.ForeignKey(Jobsite, related_name="bookings", on_delete=models.PROTECT,
                                verbose_name=_("Jobsite"))
    supervisor = models.ForeignKey(ClientContact, related_name="bookings", on_delete=models.PROTECT,
                                   verbose_name=_("Supervisor"))
    superviser_signed_at = models.DateTimeField(null=True, blank=True,
                                               verbose_name=_("Supervisor Signed at"))
    employee_signed_at = models.DateTimeField(null=True, blank=True,
                                                verbose_name=_("Employee Signed at"))
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    bookings_invoice = models.ForeignKey(BookingsInvoice, on_delete=models.PROTECT,
                                         related_name="bookings",
                                         verbose_name=_("Bookings Invoice"))

    class Meta:
        verbose_name = _("Booking")
        verbose_name_plural = _("Bookings")

    def __str__(self):
        return '{}/{}: {} - {}'.format(self.client, self.jobsite, self.employee_contact,
                                       self.updated_at)


class BookingState(models.Model):
    STATE_CHOICES = Choices(
        (10, 'new', _("New")),
        (20, 'searching_for_employee', _("Searching for Employee")),
        (30, 'employee_found', _("Employee Found")),
        (40, 'employee_accepted', _("Employee Accepted")),
        (50, 'open', _("Open")),
        (60, 'suspended', _("Suspended")),
        (70, 'work_completed', _("Work Completed")),
        (90, 'invoiced', _("Invoiced")),
        (0, 'cancelled', _("Cancelled")),
    )

    state = models.PositiveSmallIntegerField(verbose_name=_("State"), choices=STATE_CHOICES,
                                             default=STATE_CHOICES.new)
    booking = models.ForeignKey(Booking, related_name="booking_states", on_delete=models.PROTECT,
                                verbose_name=_("Booking"))
    updated_by = models.ForeignKey(Contact, related_name="booking_state_updates",
                                   on_delete=models.PROTECT, verbose_name=_("Updated by"))
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    notes = models.TextField(verbose_name=_("Notes"), help_text=_("State Change Description"),
                             blank=True)

    class Meta:
        verbose_name = _("Booking State")
        verbose_name_plural = _("Booking States")

    def __str__(self):
        return self.state


class TimeSheetEntry(models.Model):
    WEEKDAY_CHOICES = Choices(
        (1, 'monday', _("Monday")),
        (2, 'tuesday', _("Tuesday")),
        (3, 'wednesday', _("Wednesday")),
        (4, 'thursday', _("Thursday")),
        (5, 'friday', _("Friday")),
        (6, 'saturday', _("Saturday")),
        (7, 'sunday', _("Sunday")),
    )

    RATE_OF_EMPLOYEE_CHOICES = Choices(
        (1, 'poor', _("Poor")),
        (2, 'mediocre', _("Mediocre")),
        (3, 'good', _("Good")),
        (4, 'great', _("Great")),
        (5, 'excellent', _("Excellent")),
    )

    booking = models.ForeignKey(Booking, on_delete=models.PROTECT, related_name="timesheet_entries",
                                verbose_name=_("Booking"))
    shift_start_date = models.DateField(verbose_name=_("Shift start date"), auto_now_add=True)
    shift_start_weekday = models.PositiveSmallIntegerField(verbose_name=_("Weekday"),
                                                           default=1) #FIXME:date.today.isoweekday
    shift_started_at = models.DateTimeField(verbose_name=_("Shift Started at"))
    shift_ended_at = models.DateTimeField(verbose_name=_("Shift Ended at"), null=True, blank=True)
    break_started_at = models.DateTimeField(verbose_name=_("Break Started at"), null=True,
                                            blank=True)
    break_ended_at = models.DateTimeField(verbose_name=_("Break Ended at"), null=True, blank=True)
    client = models.ForeignKey(Client, related_name="timesheet_entries", on_delete=models.PROTECT,
                               verbose_name=_("Client"))
    employee_contact = models.ForeignKey(EmployeeContact, on_delete=models.PROTECT,
                                         related_name="timesheet_entries",
                                         verbose_name=_("Employee Contact"))
    position = models.ForeignKey(EmployeeSkill, on_delete=models.PROTECT,
                                       related_name="employee_entries",
                                       verbose_name=_("Position"))
    jobsite = models.ForeignKey(Jobsite, related_name="timesheet_entries", on_delete=models.PROTECT,
                                verbose_name=_("Jobsite"))
    supervisor = models.ForeignKey(ClientContact, related_name="timesheet_entries",
                                   on_delete=models.PROTECT, verbose_name=_("Supervisor"))
    superviser_signed_at = models.DateTimeField(null=True, blank=True,
                                                verbose_name=_("Supervisor Signed at"))
    employee_signed_at = models.DateTimeField(null=True, blank=True,
                                              verbose_name=_("Employee Signed at"))
    rate_of_employee = models.PositiveSmallIntegerField(verbose_name=_("Rate our staff"),
                                                        choices=RATE_OF_EMPLOYEE_CHOICES,
                                                        default=RATE_OF_EMPLOYEE_CHOICES.good)

    class Meta:
        verbose_name = _("Booking Timesheet Entry")
        verbose_name_plural = _("Booking Timesheet Entries")
        unique_together = ("booking", "shift_start_date")

    def __str__(self):
        return '{}: {}/{}'.format(self.booking, self.shift_start_date, self.shift_start_weekday)


class BookingIssues(models.Model):
    booking = models.ForeignKey(Booking, on_delete=models.PROTECT, related_name="booking_issues",
                                verbose_name=_("Booking"))
    subject = models.CharField(max_length=255, verbose_name=_("Subject"))
    description = models.TextField(verbose_name=_("Description"))
    supervisor = models.ForeignKey(ClientContact, related_name="booking_issues",
                                   on_delete=models.PROTECT, verbose_name=_("Supervisor"))
    superviser_signed_at = models.DateTimeField(null=True, blank=True,
                                                verbose_name=_("Supervisor Signed at"))
    agency_contact = models.ForeignKey(AgencyContact, related_name="booking_issues",
                                       verbose_name=_("Agency Contact Responsible"),
                                       on_delete=models.PROTECT, null=True)

    class Meta:
        verbose_name = _("Booking Issue")
        verbose_name_plural = _("Booking Issues")

    def __str__(self):
        return '{}: {}/{}'.format(self.booking, self.shift_start_date, self.shift_start_weekday)


class BookingIssueState(models.Model):
    STATE_CHOICES = Choices(
        (10, 'new', _("New")),
        (20, 'open', _("Open")),
        (30, 'employee_input', _("Employee Input")),
        (40, 'client_input', _("Client Input")),
        (50, 'agency_response', _("Agency Response")),
        (60, 'suspended', _("Suspended")),
        (70, 'resolved', _("Resolved")),
        (0, 'cancelled', _("Cancelled")),
    )

    state = models.PositiveSmallIntegerField(verbose_name=_("State"), choices=STATE_CHOICES,
                                             default=STATE_CHOICES.new)
    booking_issue = models.ForeignKey(BookingIssues, related_name="booking_issue_states",
                                      on_delete=models.PROTECT, verbose_name=_("Booking Issue"))
    updated_by = models.ForeignKey(Contact, related_name="booking_issue_state_updates",
                                   on_delete=models.PROTECT, verbose_name=_("Updated by"))
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    notes = models.TextField(verbose_name=_("Notes"), help_text=_("State Change Description"),
                             blank=True)

    class Meta:
        verbose_name = _("Booking Issue State")
        verbose_name_plural = _("Booking Issue States")

    def __str__(self):
        return self.state

from django import forms
from mptt.forms import TreeNodeChoiceField
from .models import Element

class elementAdminForm(forms.ModelForm):
    element = TreeNodeChoiceField(queryset=Element.objects.all(), level_indicator=u'-')

import os
import json

from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.core.urlresolvers import reverse

from .utils import load_config, render_content
from .models import Layout, Element, Page, ContentItem


def manifest(request):
    manifest = load_config("manifest")
    try:
        fi = open(os.path.dirname(__file__)+'/static/'+manifest["icons"], 'r')
    except Exception:
        pass    #imporant - to ignore file not found exception in production
    else:
        manifest["icons"] = json.loads(fi.read())
        fi.close()
    return JsonResponse(manifest)


def home(request):
    return page(request, slug="home")


def page(request, slug):
    meta = {}
    meta["app"] = load_config("meta_app")
    meta["app_colors"] = load_config("meta_app_colors")
    meta["manifest"] = load_config("manifest")

    page = get_object_or_404(Page, slug=slug)
    if len(page.title) > 0:
        meta["app"]["title"] = '{} {}'.format(page.title, meta["app"]["title"])
    if len(page.description) > 0:
        meta["app"]["description"] = page.description

    layout = get_object_or_404(Layout, pk=page.layout.pk)
    elements = Element.objects.filter(layout=layout.pk, parent=None)

    content = ContentItem.objects.filter(page=page.pk)
    eci = {} #Element Content Index
    for c in content:
        eci[c.element.pk] = c

    #Render with Content
    rc = ''
    for element in elements:
        rc += render_content(element, page, content, eci)

    return render(request, "page.jinja", {"meta": meta, "content": rc})


def polymer_component(request, name):
    return render(request, "components/"+name+".jinja", {})

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^manifest.json$', views.manifest, name='manifest'),
    url(r'^polymer_component/(?P<name>\w+)/$', views.polymer_component, name='polymer_component'),
]

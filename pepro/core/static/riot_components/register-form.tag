<register-form>
    <div class="field-group">
        <input name="username" class={"field": true, "field--error": errors} type="text" placeholder="Username" />
        <input name="email" class={"field": true, "field--error": errors} type="email" placeholder="Email" />
        <input name="password1" class={"field": true, "field--error": errors} type="password" placeholder="Password" />
        <input name="password2" class={"field": true, "field--error": errors} type="password" placeholder="Password (again)" />
        <button class="button button--primary" onclick={submit}>Signup</button>
    </div>
    <script>
        var self = this;
        this.errors = false;
        this.submit = function(e) {
            $.post("/data/user/register/", {
                "username": this.username.value,
                "email": this.email.value,
                "password1": this.password1.value,
                "password2": this.password2.value,
            }).done(function(data) {
                self.errors = false;
                window.location = "/";
            }).fail(function() {
                self.errors = true;
                self.update();
            });
        }
    </script>
</register-form>

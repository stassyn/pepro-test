<login-form>
    <div class="field-group">
        <input name="login" class={"field": true, "field--error": errors} type="text" placeholder="Login" />
        <input name="password" class={"field": true, "field--error": errors} type="password" placeholder="Password" />
        <button class="button button--primary" onclick={submit}>Login</button>
    </div>
    <script>
        var self = this;
        this.errors = false;
        this.submit = function(e) {
            $.post("/data/user/login/", {
                "login": this.login.value,
                "password": this.password.value,
                "remember": true,
            }).done(function(data) {
                self.errors = false;
                window.location = "/";
            }).fail(function() {
                self.errors = true;
                self.update();
            });
        }
    </script>
</login-form>

<top-login-register>
    <!-- -*- mode: html; tab-width: 4 -*- -->
    <div if={ opts.profile }>
        <a href="/accounts/profile" class="link">{ opts.profile.hide_username ? "My profile" : opts.profile.username }</a> | <a href="/user/logout/" class="link">Logout</a>
    </div>
    <div if={ !opts.profile }>
        <a href="/user/login/" class="link" onclick={event(actions.login)}>Login</a> | <a class="link" href="/user/register/" onclick={event(actions.register)}>Register</a>
    </div>
    <script>
        var self = this;
        this.actions = opts.actions;
        this.event = function(action) {
            return function(e) {
                if (e.button != 0)
                    return true;
                action(self);
            }
        }
    </script>
</top-login-register>

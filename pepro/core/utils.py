from django.shortcuts import get_object_or_404
from django.utils import translation
from .models import Config, ConfigParam


def load_config(config_name):
    config = get_object_or_404(Config, name=config_name)
    params = ConfigParam.objects.filter(config=config.pk)
    res = {}
    for cp in params:
        res[cp.name] = cp.value
    return res


def render_content(element, page, content, eci):
    res = '<{} {} id="{}"'.format(element.tag, element.attributes_output(), element.name)
    for child in element.children.all():
        res += render_content(child, page, content, eci)
    if eci.has_key(element.pk) and len(eci[element.pk].content) > 0:
        res += str(eci[element.pk].content)
    else:
        res += str(element.content)
    res += '</{}>'.format(element.tag)
    return res


def translation_context_processor(request):
    return {
        'get_language_info': translation.get_language_info,
    }

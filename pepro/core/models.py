from __future__ import unicode_literals
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.utils.translation import ugettext_lazy as _


class Config(models.Model):
    name = models.SlugField(max_length=64, verbose_name=_("Machine Name"), db_index=True)
    title = models.CharField(max_length=250, verbose_name=_("Configuration Title"))

    class Meta:
        verbose_name = _("Application Configuration")
        verbose_name_plural = _("Application Configurations")
    def __str__(self):
        return self.name


class ConfigParam(models.Model):
    name = models.SlugField(max_length=64, verbose_name=_("Parameter Name"), db_index=True)
    value = models.CharField(max_length=250, verbose_name=_("Parameter Value"))
    config = models.ForeignKey(Config, verbose_name=_("Configuration"),
                               related_name="config_params", related_query_name="config_params",
                               null=False)

    class Meta:
        verbose_name = _("Application Configuration Parameter")
        verbose_name_plural = _("Application Configuration Parameters")
    def __str__(self):
        return '{}: {}'.format(self.name, self.value)


class Layout(models.Model):
    name = models.SlugField(max_length=64, verbose_name=_("Layout Machine Name"), db_index=True)
    title = models.CharField(max_length=64, verbose_name=_("Layout Title"))

    class Meta:
        verbose_name = _("Layout")
        verbose_name_plural = _("Layouts")

    def element_count(self):
        return self.elements.count()

    def __str__(self):
        return self.name


class Element(MPTTModel):
    tag = models.SlugField(max_length=64, verbose_name=_("Element Tag"), db_index=True)
    name = models.SlugField(max_length=64, verbose_name=_("Element Machine Name"), db_index=True)
    layout = models.ForeignKey(Layout, verbose_name=_("Layout"), related_name="elements",
                               related_query_name="elements", db_index=True)
    weight = models.BigIntegerField(verbose_name=_("Weight"), default=0, null=False, blank=False,
                                    db_index=True)
    parent = TreeForeignKey('self', verbose_name=_("Parent"), null=True, blank=True,
                            related_name='children', db_index=True)
    content = models.TextField(verbose_name=_("Content"), blank=True)

    class Meta:
        verbose_name = _("Layout Element")
        verbose_name_plural = _("Layout Elements")
        unique_together = ("layout", "name")

    class MPTTMeta:
        order_insertion_by = ["layout", "weight", "name"]

    def attributes_count(self):
        return self.attributes.count()

    def attributes_output(self):
        res = ""
        i = 0
        for attr in self.attributes.all():
            if i > 0:
                res += " "
            res += attr.name
            if len(attr.value) > 0:
                res += "=\""+attr.value+"\""
            i += 1
        return str(res)

    def render(self):
        res = '<{} {} id="{}">'.format(self.tag, self.attributes_output(), self.name)
        for child in self.children.all():
            res += child.render()
        res += str(self.content)
        res += '</{}>'.format(self.tag)
        return res

    def __str__(self):
        return '<{} id="{}" layout="{}">'.format(self.tag, self.name, self.layout.name)


class Attribute(models.Model):
    name = models.SlugField(max_length=64, verbose_name=_("Element Machine Name"), db_index=True)
    value = models.CharField(max_length=250, verbose_name=_("Attribute Value"), blank=True)
    element = models.ForeignKey(Element, verbose_name=_("Element"), related_name="attributes",
                                db_index=True)

    class Meta:
        verbose_name = _("Layout Element Attribute")
        verbose_name_plural = _("Layout Element Attributes")
        unique_together = ("element", "name")

    def __str__(self):
        return self.name


class Page(models.Model):
    slug = models.SlugField(max_length=64, verbose_name=_("Page Slug"), db_index=True,
                            help_text=_("Machine Name"))
    title = models.CharField(max_length=64, verbose_name=_("Page Title"))
    layout = models.ForeignKey(Layout, verbose_name=_("Layout"), related_name="pages")
    description = models.CharField(max_length=156, verbose_name=_("Meta Description"), blank=True)

    class Meta:
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")

    def __str__(self):
        return self.slug


class ContentItem(models.Model):
    content = models.TextField(verbose_name=_("Content"), blank=True)
    page = models.ForeignKey(Page, verbose_name=_("Page"), related_name="content_items", db_index=True)
    element = models.ForeignKey(Element, verbose_name=_("Element"),
                                related_name="content_items", db_index=True)

    class Meta:
        verbose_name = _("Page Content")
        verbose_name_plural = _("Page Contents")
        unique_together = ("page", "element")

    def __str__(self):
        return str(self.page.slug+": "+self.element.name)

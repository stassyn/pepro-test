from django.contrib import admin
from django.db import models
from mptt.admin import MPTTModelAdmin
from . import models
from .forms import elementAdminForm

class ConfigParamInline(admin.TabularInline):
    model = models.ConfigParam
    fields = ('name', 'value')
    extra = 0
    min_num = 1

class ConfigAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'name')
    list_display_links = ('name', 'title')
    inlines = (ConfigParamInline,)

class LayoutAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'title', 'element_count')
    list_display_links = ('name',)

class AttributeInline(admin.TabularInline):
    model = models.Attribute
    fields = ('name', 'value')
    extra = 0
    min_num = 0

class ElementAdmin(MPTTModelAdmin):
    inlines = (AttributeInline,)
    list_display = ('id', 'tag', 'name', 'attributes_output', 'layout')
    list_display_links = ('tag', 'name')
    list_filter = ('layout',)
    search_fields = ('tag', 'name')

class ContentInline(admin.TabularInline):
    form = elementAdminForm
    model = models.ContentItem
    fields = ('element', 'content')
    extra = 1
    min_num = 0

class PageAdmin(admin.ModelAdmin):
    search_fields = ('slug', 'title', 'description')
    list_display = ('id', 'slug', 'title', 'layout')
    list_display_links = ('slug',)
    inlines = (ContentInline,)

admin.site.site_header += ' | Endless PRO'
admin.site.register(models.Config, ConfigAdmin)
admin.site.register(models.Layout, LayoutAdmin)
admin.site.register(models.Element, ElementAdmin)
admin.site.register(models.Page, PageAdmin)

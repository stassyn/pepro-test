import os
import sys
from django.utils.translation import ugettext_lazy as _

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

env = lambda key: os.environ.get(key)

SECRET_KEY = env('DJANGO_SECRET_KEY')
if env('DJANGO_DEBUG') == '1':
    DEBUG = True
else:
    DEBUG = False

TESTING = len(sys.argv) > 1 and sys.argv[1] == 'test'

SITE_ID = 1

ALLOWED_HOSTS = ['*']

# Application definition
INSTALLED_APPS = [
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_jinja',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'djcelery',
    'pipeline',
    'mptt',
    'django_extensions',
    'phonenumber_field',
    'django_countries',
    'pepro.core',
    'pepro.crm_core',
    'pepro.crm_hr',
    'pepro.zze_sandbox',
]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'pepro.urls'

CONTEXT_PROCESSORS = (
    'django.template.context_processors.debug',
    'django.template.context_processors.request',
    'django.template.context_processors.i18n',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'pepro.core.utils.translation_context_processor',
)

from django_jinja.builtins import DEFAULT_EXTENSIONS

TEMPLATES = [
    {
        "BACKEND": "django_jinja.backend.Jinja2",
        "APP_DIRS": True,
        "OPTIONS": {
            'extensions': DEFAULT_EXTENSIONS + [
                'pipeline.jinja2.PipelineExtension'
            ],
            'match_extension': '.jinja',
            'context_processors': CONTEXT_PROCESSORS,
        }
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': CONTEXT_PROCESSORS,
        },
    },
]

WSGI_APPLICATION = 'pepro.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env('POSTGRES_DB_NAME'),
        'USER': env('POSTGRES_USERNAME'),
        'PASSWORD': env('POSTGRES_PASSWORD'),
        'HOST': env('POSTGRES_HOST'),
        'PORT': str(env('POSTGRES_PORT')),
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Django AllAuth Configuration
# http://django-allauth.readthedocs.io/en/latest/index.html
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

ACCOUNT_ADAPTER = 'pepro.crm_core.adapter.CustomAccountAdapter'

ACCOUNT_EMAIL_REQUIRED = True

ACCOUNT_AUTHENTICATION_METHOD = "username_email"

ACCOUNT_CONFIRM_EMAIL_ON_GET = True

ACCOUNT_EMAIL_CONFIRMATION_ANONYMOUS_REDIRECT_URL = '/'

ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL = '/'

ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION = True

ACCOUNT_TEMPLATE_EXTENSION = 'jinja'

ACCOUNT_LOGOUT_ON_GET = True

LOGIN_REDIRECT_URL = '/'

LOGIN_URL = '/user/login'

# Email configuration
# https://docs.djangoproject.com/ja/1.9/topics/email/
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = 'smtp.gmail.com'

EMAIL_HOST_PASSWORD = 'Mymail123'

EMAIL_HOST_USER = 'cods.max@gmail.com'

EMAIL_PORT = 587

EMAIL_USE_TLS = True


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGES = [
    ('en-au', _('Australian English')),
]
LOCALE_PATHS = (os.path.join(os.path.dirname(__file__), '../locale/'),)

USE_I18N = True
USE_L10N = True
USE_TZ = True
TIME_ZONE = "Australia/Sydney"

# Logging
RAVEN_CONFIG = {
    'dsn': 'http://685e6fbee9514ca28f8974597de8dc3d:d768fe63d9dd45998d64b9551c2b57e0@sentry.505.lt/16',
}
if DEBUG:
    RAVEN_CONFIG['dsn'] = None

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}


if not DEBUG:
    INSTALLED_APPS += ['raven.contrib.django.raven_compat']
    LOGGING['handlers']['sentry'] = {
        'level': 'ERROR',
        'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
    }
    LOGGING['root'] = {
        'level': 'ERROR',
        'handlers': ['sentry'],
    }


# Testing

if TESTING:
    class DisableMigrations(object):

        def __contains__(self, item):
            return True

        def __getitem__(self, item):
            return "notmigrations"

    MIGRATIONS_MODULES = DisableMigrations()

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'

STATICFILES_FINDERS = [
    'pipeline.finders.CachedFileFinder',
    'pipeline.finders.PipelineFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'pepro.pipelinehelpers.RiotStyleFileSystemFinder',
]

if DEBUG:
    STATICFILES_FINDERS = [
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
        'pipeline.finders.FileSystemFinder',
        'pipeline.finders.CachedFileFinder',
        'pipeline.finders.PipelineFinder',
        'pepro.pipelinehelpers.RiotStyleFileSystemFinder',
    ]

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'var', 'www', 'media')

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'var', 'www', 'static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'pepro', 'core', 'static'),
    os.path.join(BASE_DIR, 'node_modules'),
    os.path.join(BASE_DIR, 'bower_components'),
)

PIPELINE = {
    'COMPILERS': (
        'pipeline.compilers.sass.SASSCompiler',
        'pepro.pipelinehelpers.RiotJsCompiler',
        'pepro.pipelinehelpers.RiotCssCompiler',
    ),
    'JAVASCRIPT': {
        'top': {
            'source_filenames': (
                'jquery/dist/jquery.js',
                'js-cookie/src/js.cookie.js',
                'js/jquery.ajaxcsrf.js',
                'riot/riot.js',
                'riotcontrol/riotcontrol.js',
                'riotgear/dist/rg.js',
                'riotgear-router/dist/rg-router.js',
                'reflux/dist/reflux.js',
            ),
            'output_filename': 'js/top.js',
        },
        'riot_components': {
            'source_filenames': (
                'riot_components/sample.tag',
                'riot_components/top-menu.tag',
                'riot_components/top-login-register.tag',
                'riot_components/login-form.tag',
                'riot_components/register-form.tag',
            ),
            'output_filename': 'js/components.js',
        }
    },
    'STYLESHEETS': {
        'main': {
            'source_filenames': (
                'blaze/dist/blaze.min.css',
                'scss/base.scss',
                'riot_components/sample.tag!style',
                'riot_components/top-menu.tag!style',
            ),
            'output_filename': 'css/main.css',
        },
    },
}

SCSS_BASE = os.path.join(BASE_DIR, 'pepro', 'core', 'static', 'scss')
PIPELINE['CSS_COMPRESSOR'] = 'pipeline.compressors.cssmin.CSSMinCompressor'
PIPELINE['CSSMIN_BINARY'] = os.path.join(BASE_DIR, 'node_modules/cssmin/bin/cssmin')
PIPELINE['SASS_BINARY'] = '/usr/bin/env sass'
PIPELINE['SASS_ARGUMENTS'] = '--load-path {}'.format(SCSS_BASE)

PIPELINE['JS_COMPRESSOR'] = 'pipeline.compressors.NoopCompressor'
PIPELINE['RIOT_BINARY'] = os.path.join(BASE_DIR, 'node_modules/riot/node_modules/.bin/riot')
PIPELINE['RIOT_JS_ARGUMENTS'] = ['--type', 'es6', '--exclude', 'css', '--whitespace']
PIPELINE['RIOT_CSS_ARGUMENTS'] = ['--export', 'css', '--whitespace']

CELERY_RESULT_BACKEND = 'djcelery.backends.database:DatabaseBackend'
BROKER_URL = 'redis://localhost:{port}/{db}'.format(
    port=env('REDIS_PORT'),
    db=env('REDIS_DB'),
)

# Phone Number Settings
PHONENUMBER_DB_FORMAT = 'E164'

import djcelery
djcelery.setup_loader()

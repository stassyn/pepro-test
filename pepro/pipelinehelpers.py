import os
import shutil

from django.conf import settings
from pipeline.finders import AppDirectoriesFinder

from pipeline.compilers.sass import SASSCompiler
from pipeline.compilers import SubProcessCompiler


class RiotCompiler(SubProcessCompiler):

    def match_file(self, filename):
        return filename.endswith('.{}'.format(self.input_extension))

    def prepare_infile_name(self, infile):
        return infile

    def compile_file(self, infile, outfile, outdated=False, force=False):
        if not outdated and not force:
            return

        infile = self.prepare_infile_name(infile)
        command = [
            settings.PIPELINE['RIOT_BINARY'],
            infile,
            outfile,
        ]
        command += self.get_extra_arguments()
        return self.execute_command(command, cwd=os.path.dirname(infile))


class RiotJsCompiler(RiotCompiler):
    input_extension = 'tag'
    output_extension = 'js'

    def get_extra_arguments(self):
        return getattr(settings, 'PIPELINE', {}).get('RIOT_JS_ARGUMENTS', [])


class RiotCssCompiler(RiotCompiler, SASSCompiler):
    input_extension = 'tag!style'
    output_extension = 'css'

    def prepare_infile_name(self, infile):
        return self.get_real_path(infile)

    def get_extra_arguments(self):
        return getattr(settings, 'PIPELINE', {}).get('RIOT_CSS_ARGUMENTS', [])

    def compile_file(self, infile, outfile, outdated=False, force=False):
        infile = self.prepare_infile_name(infile)
        scss_path = outfile.replace('.css', '.scss')

        # Extract scss from riot .tag module
        RiotCompiler.compile_file(self, infile, outfile, outdated=outdated, force=force)

        # Move .css file to .scss, because riot compile script appends .css
        # extension to file name even when other extension is specified
        shutil.move(outfile, scss_path)

        # Compile .scss to .css
        return SASSCompiler.compile_file(self, scss_path, outfile, outdated=outdated, force=force)

    @classmethod
    def get_real_path(cls, path):
        return path.replace('!style', '')


class RiotStyleFileSystemFinder(AppDirectoriesFinder):
    """Custom FileSystemFinder to find pseudo files with .tag!style extension.

    It is needed, because django-pipeline applies only one `compiler` per file,
    so if the same file contains both scripts and styles, only one compiler will be applied.

    Workaround is to append !style suffix to riot component file name, so other `compiler`
    could be applied."""

    def find(self, path, all=False):
        path = RiotCssCompiler.get_real_path(path)
        return super().find(path, all=all)

    def find_location(self, root, path, prefix=None):
        path = RiotCssCompiler.get_real_path(path)
        res = super().find_location(root, path, prefix=prefix)
        if res:
            return RiotCssCompiler.get_real_path(res)
